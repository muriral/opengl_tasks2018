#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

glm::vec3 makeNormalVec(float a, float b, float c, float u, float v) {
    float cos2v = cos(v) * cos(v);
    float sin2v = sin(v) * sin(v);
    float cosh2u = cosh(u) * cosh(u);
    float sinh2u = sinh(u) * sinh(u);
    
    float k = 1.0 / sqrt(b*b*c*c*sinh2u*sinh2u*cos2v + a*a*c*c*sinh2u*sinh2u*sin2v + a*a*b*b*sinh2u*cosh2u);
    float x = k*(-b*c*sinh2u*cos(v));
    float y = k*(-a*c*sinh2u*sin(v));
    float z = k*(a*b*sinh(u)*cosh(u));
    
    return glm::normalize(glm::vec3(x, y, z));
}

MeshPtr makeHyperboloid (float a, float b, float c, int n) {
    
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    
    for (int i = 0; i < n; i++)
    {
        float z = (float)i / n * 2;
        float z1 = z + 2.0 / n;
        float u = z / c;
        float u1 = z1 / c;
        
        for (int j = 0; j < n; j++)
        {
            float v = 2.0 * glm::pi<float>() * j / n;
            float v1 = v + 2.0 * glm::pi<float>() / n;
            
            //q(u,v)
            vertices.push_back(glm::vec3(a * cos(v) * sqrt(u * u - 1),
                                         b * sin(v) * sqrt(u * u - 1),
                                         c * u));
            vertices.push_back(glm::vec3(a * cos(v1) * sqrt(u * u - 1),
                                         b * sin(v1) * sqrt(u * u - 1),
                                         c * u));
            vertices.push_back(glm::vec3(a * cos(v) * sqrt(u1 * u1 - 1),
                                         b * sin(v) * sqrt(u1 * u1 - 1),
                                         c * u1));
            vertices.push_back(glm::vec3(a * cos(v1) * sqrt(u1 * u1 - 1),
                                         b * sin(v1) * sqrt(u1 * u1 - 1),
                                         c * u1));
            vertices.push_back(glm::vec3(a * cos(v1) * sqrt(u * u - 1),
                                         b * sin(v1) * sqrt(u * u - 1),
                                         c * u));
            vertices.push_back(glm::vec3(a * cos(v) * sqrt(u1 * u1 - 1),
                                         b * sin(v) * sqrt(u1 * u1 - 1),
                                         c * u1));
            
            normals.push_back(makeNormalVec(a, b, c, u, v));
            normals.push_back(makeNormalVec(a, b, c, u, v1));
            normals.push_back(makeNormalVec(a, b, c, u1, v));
            normals.push_back(makeNormalVec(a, b, c, u1, v1));
            normals.push_back(makeNormalVec(a, b, c, u, v1));
            normals.push_back(makeNormalVec(a, b, c, u1, v));
        }
        
        u = -z / c;
        u1 = -z1 / c;
        
        for (int j = 0; j < n; j++)
        {
            float v = 2.0 * glm::pi<float>() * j / n;
            float v1 = v + 2.0 * glm::pi<float>() / n;
            
            vertices.push_back(glm::vec3(a * cos(v) * sqrt(u * u - 1),
                                         b * sin(v) * sqrt(u * u - 1),
                                         c * u));
            vertices.push_back(glm::vec3(a * cos(v1) * sqrt(u * u - 1),
                                         b * sin(v1) * sqrt(u * u - 1),
                                         c * u));
            vertices.push_back(glm::vec3(a * cos(v) * sqrt(u1 * u1 - 1),
                                         b * sin(v) * sqrt(u1 * u1 - 1),
                                         c * u1));
            vertices.push_back(glm::vec3(a * cos(v1) * sqrt(u1 * u1 - 1),
                                         b * sin(v1) * sqrt(u1 * u1 - 1),
                                         c * u1));
            vertices.push_back(glm::vec3(a * cos(v1) * sqrt(u * u - 1),
                                         b * sin(v1) * sqrt(u * u - 1),
                                         c * u));
            vertices.push_back(glm::vec3(a * cos(v) * sqrt(u1 * u1 - 1),
                                         b * sin(v) * sqrt(u1 * u1 - 1),
                                         c * u1));
            
            normals.push_back(makeNormalVec(a, b, c, u, v));
            normals.push_back(makeNormalVec(a, b, c, u, v1));
            normals.push_back(makeNormalVec(a, b, c, u1, v));
            normals.push_back(makeNormalVec(a, b, c, u1, v1));
            normals.push_back(makeNormalVec(a, b, c, u, v1));
            normals.push_back(makeNormalVec(a, b, c, u1, v));
        }
    }
    
    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());
    
    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());
    
    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());
    
    return mesh;
}

class SampleApplication : public Application
{
public:
    MeshPtr _twoSheetHyperboloid;
    
    ShaderProgramPtr _shader;
    
    float _polygonsCount = 60.0f;
    int _min = 10;
    int _max = 100;
    float step_t = 0.1;
    float _a = 0.1f, _b = 0.1f, _c = 0.1f;
    
    void makeScene() override
    {
        Application::makeScene();
        
        _cameraMover = std::make_shared<FreeCameraMover>();
        
        //Создаем меш
        _twoSheetHyperboloid = makeHyperboloid(_a, _b, _c, (int)_polygonsCount);
        
        //Создаем шейдерную программу
        _shader = std::make_shared<ShaderProgram>("497MudrovaData/shaderNormal.vert", "497MudrovaData/shader.frag");
    }
    
    void update() override
    {
        Application::update();
    }
    
    void draw() override
    {
        Application::draw();
        
        //Получаем размеры экрана (окна)
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        
        //Устанавливаем порт вывода на весь экран (окно)
        glViewport(0, 0, width, height);
        
        //Очищаем порт вывода (буфер цвета и буфер глубины)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        //Устанавливаем шейдер
        _shader->use();
        
        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        
        //Рисуем первый меш
        _shader->setMat4Uniform("modelMatrix", _twoSheetHyperboloid->modelMatrix());
        _twoSheetHyperboloid->draw();
    }
    
    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);
        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_MINUS)
            {
                _polygonsCount = std::max(float(_min), _polygonsCount - _polygonsCount * step_t);
                _twoSheetHyperboloid = makeHyperboloid(_a, _b, _c, (int)_polygonsCount);
            }
            if (key == GLFW_KEY_EQUAL)
            {
                _polygonsCount = std::min(float(_max), _polygonsCount + _polygonsCount * step_t);
                _twoSheetHyperboloid = makeHyperboloid(_a, _b, _c, (int)_polygonsCount);
            }
        }
    }
};

int main()
{
    SampleApplication app;
    app.start();
    
    return 0;
}