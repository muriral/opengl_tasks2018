#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

glm::vec3 makeNormalVec(float a, float b, float c, float u, float v) {
	float cos2v = cos(v) * cos(v);
	float sin2v = sin(v) * sin(v);
	float cosh2u = cosh(u) * cosh(u);
	float sinh2u = sinh(u) * sinh(u);

	float k = 1.0 / sqrt(b*b*c*c*sinh2u*sinh2u*cos2v + a*a*c*c*sinh2u*sinh2u*sin2v + a*a*b*b*sinh2u*cosh2u);
	float x = k*(-b*c*sinh2u*cos(v));
	float y = k*(-a*c*sinh2u*sin(v));
	float z = k*(a*b*sinh(u)*cosh(u));

	return glm::normalize(glm::vec3(x, y, z));
}

MeshPtr makeHyperboloid (float a, float b, float c, int n) {

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;

	for (int i = 0; i < n; i++)
	{
		float z = (float)i / n * 2;
		float z1 = z + 2.0 / n;
		float u = z / c;
		float u1 = z1 / c;
		
		for (int j = 0; j < n; j++)
		{
			float v = 2.0 * glm::pi<float>() * j / n;
			float v1 = v + 2.0 * glm::pi<float>() / n;

			//q(u,v)
            vertices.push_back(glm::vec3(a * cos(v) * sqrt(u * u - 1),
										 b * sin(v) * sqrt(u * u - 1),
										 c * u));
            vertices.push_back(glm::vec3(a * cos(v1) * sqrt(u * u - 1),
										 b * sin(v1) * sqrt(u * u - 1),
										 c * u));
            vertices.push_back(glm::vec3(a * cos(v) * sqrt(u1 * u1 - 1),
										 b * sin(v) * sqrt(u1 * u1 - 1),
										 c * u1));
            vertices.push_back(glm::vec3(a * cos(v1) * sqrt(u1 * u1 - 1),
										 b * sin(v1) * sqrt(u1 * u1 - 1),
										 c * u1));
            vertices.push_back(glm::vec3(a * cos(v1) * sqrt(u * u - 1),
										 b * sin(v1) * sqrt(u * u - 1),
										 c * u));
            vertices.push_back(glm::vec3(a * cos(v) * sqrt(u1 * u1 - 1),
										 b * sin(v) * sqrt(u1 * u1 - 1),
										 c * u1));

			normals.push_back(makeNormalVec(a, b, c, u, v));
			normals.push_back(makeNormalVec(a, b, c, u, v1));
			normals.push_back(makeNormalVec(a, b, c, u1, v));
			normals.push_back(makeNormalVec(a, b, c, u1, v1));
			normals.push_back(makeNormalVec(a, b, c, u, v1));
			normals.push_back(makeNormalVec(a, b, c, u1, v));
		}

		u = -z / c;
		u1 = -z1 / c;

		for (int j = 0; j < n; j++)
		{
			float v = 2.0 * glm::pi<float>() * j / n;
			float v1 = v + 2.0 * glm::pi<float>() / n;

            vertices.push_back(glm::vec3(a * cos(v) * sqrt(u * u - 1),
										 b * sin(v) * sqrt(u * u - 1),
										 c * u));
            vertices.push_back(glm::vec3(a * cos(v1) * sqrt(u * u - 1),
										 b * sin(v1) * sqrt(u * u - 1),
										 c * u));
            vertices.push_back(glm::vec3(a * cos(v) * sqrt(u1 * u1 - 1),
										 b * sin(v) * sqrt(u1 * u1 - 1),
										 c * u1));
			vertices.push_back(glm::vec3(a * cos(v1) * sqrt(u1 * u1 - 1),
										 b * sin(v1) * sqrt(u1 * u1 - 1),
										 c * u1));
            vertices.push_back(glm::vec3(a * cos(v1) * sqrt(u * u - 1),
										 b * sin(v1) * sqrt(u * u - 1),
										 c * u));
            vertices.push_back(glm::vec3(a * cos(v) * sqrt(u1 * u1 - 1),
										 b * sin(v) * sqrt(u1 * u1 - 1),
										 c * u1));

			normals.push_back(makeNormalVec(a, b, c, u, v));
			normals.push_back(makeNormalVec(a, b, c, u, v1));
			normals.push_back(makeNormalVec(a, b, c, u1, v));
			normals.push_back(makeNormalVec(a, b, c, u1, v1));
			normals.push_back(makeNormalVec(a, b, c, u, v1));
			normals.push_back(makeNormalVec(a, b, c, u1, v));
		}
	}

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(vertices.size());

	return mesh;
}

class ToonShadingApplication: public Application
{
public:
	MeshPtr _hyperboloid;
	MeshPtr _marker;

	ShaderProgramPtr _silhouette_shader;
	ShaderProgramPtr _shades_shader;
	ShaderProgramPtr _markerShader;

	float _num_polygons = 50.0f;
	float _a = 0.1f, _b = 0.1f, _c = 0.1f;

	float _lr = 3.5f;
	float _phi = 0.0f;
	float _theta = glm::pi<float>() * 0.25f;
	glm::vec3 _lightDiffuseColor;

	void makeScene() override
	{
		Application::makeScene();

		_cameraMover = std::make_shared<FreeCameraMover>();

		_hyperboloid = makeHyperboloid(_a, _b, _c, _num_polygons);
		_marker = makeSphere(0.1f);

		_silhouette_shader = std::make_shared<ShaderProgram>("497MudrovaData/silhouette.vert", "497MudrovaData/silhouette.frag");
		_shades_shader = std::make_shared<ShaderProgram>("497MudrovaData/shades.vert", "497MudrovaData/shades.frag");
		_markerShader = std::make_shared<ShaderProgram>("497MudrovaData/marker.vert", "497MudrovaData/marker.frag");

		_lightDiffuseColor = glm::vec3(0.8, 0.8, 0.8);
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("Toon shading", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{

			if (ImGui::CollapsingHeader("Light"))
			{
				ImGui::ColorEdit3("Diffuse", glm::value_ptr(_lightDiffuseColor));

				ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
				ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
				ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
			}

		}
		ImGui::End();
	}

	void draw() override
	{
		Application::draw();
		//Получаем размеры экрана (окна)
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);
		//Устанавливаем порт вывода на весь экран (окно)
		glViewport(0, 0, width, height);
		//Очищаем порт вывода (буфер цвета и буфер глубины)
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		{
			//Устанавливаем шейдер
			_silhouette_shader->use();
			glCullFace(GL_FRONT);
			glDepthMask(GL_FALSE);
			//Устанавливаем общие юниформ-переменные
			_silhouette_shader->setVec3Uniform("color", glm::vec3(0.0f, 0.0f, 0.0f));
			_silhouette_shader->setFloatUniform("normalOffset", 0.07f);
			_silhouette_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
			_silhouette_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
			_silhouette_shader->setMat4Uniform("modelMatrix", _hyperboloid->modelMatrix());
			_hyperboloid->draw();
		}

		glm::vec3 lightPosition = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;

		{
			float numShades = 7.0f;
			glm::vec3 baseColor = glm::vec3(1.0f, 0.21f, 0.0);

			_shades_shader->use();
			glEnable(GL_DEPTH_TEST);
			glDepthMask(GL_TRUE);

			_shades_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
			_shades_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
			_shades_shader->setMat4Uniform("modelMatrix", _hyperboloid->modelMatrix());
			_shades_shader->setMat3Uniform("normalMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _hyperboloid->modelMatrix()))));

			_shades_shader->setVec3Uniform("lightPosition", lightPosition);
			_shades_shader->setFloatUniform("numShades", numShades);
			_shades_shader->setVec3Uniform("baseColor", baseColor);
			_hyperboloid->draw();
		}

		{
			_markerShader->use();
			_markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), lightPosition));
			_markerShader->setVec4Uniform("color", glm::vec4(_lightDiffuseColor, 1.0f));
			_marker->draw();
		}

		glBindSampler(0, 0);
		glUseProgram(0);
	}

};

int main()
{
	ToonShadingApplication app;
	app.start();

	return 0;
}
