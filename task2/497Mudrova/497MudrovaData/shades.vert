#version 330

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;
uniform vec3 lightPosition; 

out vec3 worldNormal;
out vec3 directionToLight;
out vec3 directionToCamera; 

void main(void){

   vec4 worldPosition = modelMatrix * vec4(vertexPosition, 1.0);

   vec4 viewSpacePosition = viewMatrix * worldPosition; 
   
   worldNormal = normalize(mat3(modelMatrix) * vertexNormal);
   
   directionToCamera = normalize(-viewSpacePosition.xyz);
   
   directionToLight = normalize(lightPosition - mat3(modelMatrix) * vertexPosition);

   gl_Position = projectionMatrix * viewMatrix * worldPosition; 
}