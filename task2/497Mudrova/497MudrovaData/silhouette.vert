#version 330

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform float normalOffset;

void main(void){
   vec4 pos = vec4(vertexPosition - vertexNormal * normalOffset, 1.0);
   gl_Position = projectionMatrix*viewMatrix*modelMatrix*pos;
}