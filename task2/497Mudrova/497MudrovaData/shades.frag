#version 330

uniform vec3 baseColor; 
uniform float numShades; 

in vec3 directionToLight;
in vec3 directionToCamera;
in vec3 worldNormal; 

out vec4 fragColor;

float diffuse(vec3 L, vec3 N){
   return clamp(dot(L,N),0.0,1.0);
}

float specular(vec3 L,vec3 N,vec3 H){
   if(dot(N,L)>0){
      return pow(clamp(dot(H,N),0.0,1.0),64.0);
   }
   return 0.0;
}

void main(void){

   vec3 halfVector = normalize(directionToLight + directionToCamera);
   float ambientIntensity = 0.1f;
   float diffuseIntensity = diffuse(directionToLight, worldNormal);
   float specularIntensity = specular(directionToLight, worldNormal, halfVector);
   float totalIntensity = ambientIntensity + diffuseIntensity + specularIntensity;
   float shadeIntensity = ceil(totalIntensity * numShades)/ numShades;

   fragColor.xyz = baseColor*shadeIntensity; 
   fragColor.w = 1.0;
}